
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class AvatarGrabberGUI extends JFrame {
	private ButtonGroup buttonGroup1;
	public JButton jButton1, jButton2;
	public JFileChooser jFileChooser1;
	public JLabel jLabel1, jLabel2, jLabel3;
	public JPanel jPanel1,jPanel2;
	public JProgressBar jProgressBar1;
	public JRadioButton jRadioButton1, jRadioButton2;
	public JTextField jTextField1, jTextField2;

	public AvatarGrabberGUI() {
		initComponents();
	}

	private void initComponents() {
		
		this.jFileChooser1 = new JFileChooser();
		this.buttonGroup1 = new ButtonGroup();
		this.jPanel1 = new JPanel();
		this.jLabel2 = new JLabel();
		this.jTextField1 = new JTextField();
		this.jButton1 = new JButton();
		this.jTextField2 = new JTextField();
		this.jButton2 = new JButton();
		this.jRadioButton1 = new JRadioButton();
		this.jRadioButton2 = new JRadioButton();
		this.jProgressBar1 = new JProgressBar();
		this.jPanel2 = new JPanel();
		this.jLabel3 = new JLabel();
		this.jLabel1 = new JLabel();

		this.jFileChooser1.setFileSelectionMode(1);

		setDefaultCloseOperation(3);
		setTitle("Grabber");
		setBackground(new Color(186, 243, 117));
		setCursor(new Cursor(0));
		setForeground(new Color(186, 243, 117));
		setIconImages(null);
		setResizable(false);

		this.jPanel1.setBackground(new Color(186, 243, 117));

		this.jLabel2.setFont(new Font("Tahoma", 1, 11));
		this.jLabel2.setText("Directory");

		this.jTextField1.setEditable(false);
		this.jButton1.setText("Save");
		
		this.jTextField2.setBackground(new Color(186, 243, 117));
		this.jTextField2.setEditable(false);
		this.jTextField2.setHorizontalAlignment(4);
		this.jTextField2.setBorder(null);
		
		this.jButton2.setText("...");
		this.jButton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				AvatarGrabberGUI.this.jButton2ActionPerformed(evt);
			}
		});
		this.jRadioButton1.setBackground(new Color(186, 243, 117));
		this.jRadioButton1.setText("Firefox");
		this.jRadioButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				AvatarGrabberGUI.this.jRadioButton1ActionPerformed(evt);
			}
		});
		this.jRadioButton2.setBackground(new Color(186, 243, 117));
		this.jRadioButton2.setText("Google Chrome");
		this.jRadioButton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				AvatarGrabberGUI.this.jRadioButton2ActionPerformed(evt);
			}
		});
		GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
		this.jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(
				GroupLayout.Alignment.LEADING
			).addGroup(
				jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(
					jPanel1Layout.createParallelGroup(
						GroupLayout.Alignment.LEADING
					).addGroup(
						GroupLayout.Alignment.TRAILING,
						jPanel1Layout.createSequentialGroup().addComponent(
							this.jRadioButton2
						).addPreferredGap(
							LayoutStyle.ComponentPlacement.UNRELATED
						).addComponent(
							this.jRadioButton1
						).addGap(
							6,6,6
						).addComponent(
							this.jProgressBar1,-2,-1,-2
						).addPreferredGap(
							LayoutStyle.ComponentPlacement.RELATED,
							10,32767
						).addComponent(
							this.jButton1
						)
					).addGroup(
						GroupLayout.Alignment.TRAILING,	
						jPanel1Layout.createSequentialGroup().addComponent(
							this.jTextField1,
							-1,	344,32767
						).addGap(
							10,10,10
						).addComponent(
							this.jButton2,
							-2,	25,	-2
						)
					).addGroup(
						jPanel1Layout.createSequentialGroup().addComponent(
							this.jLabel2,
							-2,	53, -2
						).addPreferredGap(
							LayoutStyle.ComponentPlacement.RELATED,
							127, 32767
						).addComponent(
							this.jTextField2,
							-2, 199,-2
						)
					)
				).addContainerGap()));

		jPanel1Layout.setVerticalGroup(jPanel1Layout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						jPanel1Layout
								.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										jPanel1Layout
												.createParallelGroup(
														GroupLayout.Alignment.BASELINE)
												.addComponent(
														this.jLabel2)
												.addComponent(
														this.jTextField2,
														-2, -1, -2))
								.addPreferredGap(
										LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(
										jPanel1Layout
												.createParallelGroup(
														GroupLayout.Alignment.BASELINE)
												.addComponent(
														this.jTextField1,
														-2, -1, -2)
												.addComponent(
														this.jButton2))
								.addGroup(
										jPanel1Layout
												.createParallelGroup(
														GroupLayout.Alignment.LEADING)
												.addGroup(
														jPanel1Layout
																.createSequentialGroup()
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		jPanel1Layout
																				.createParallelGroup(
																						GroupLayout.Alignment.LEADING)
																				.addGroup(
																						jPanel1Layout
																								.createParallelGroup(
																										GroupLayout.Alignment.BASELINE)
																								.addComponent(
																										this.jRadioButton2)
																								.addComponent(
																										this.jRadioButton1))
																				.addComponent(
																						this.jButton1)))
												.addGroup(
														jPanel1Layout
																.createSequentialGroup()
																.addGap(11,
																		11,
																		11)
																.addComponent(
																		this.jProgressBar1,
																		-2,
																		-1,
																		-2)))
								.addContainerGap(13, 32767)));

		getContentPane().add(this.jPanel1, "Center");

		this.jPanel2.setBackground(new Color(81, 194, 52));
		this.jPanel2.setPreferredSize(new Dimension(399, 30));
		this.jPanel2.setRequestFocusEnabled(false);

		this.jLabel3.setBackground(new Color(186, 243, 117));
		this.jLabel3.setFont(new Font("Comic Sans MS", 1, 18));
		this.jLabel3.setText("Animechat.de/Avatar/Grabber");

		this.jLabel1.setFont(new Font("Tahoma", 2, 11));
		this.jLabel1.setText("by Shintso");

		GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
		this.jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				jPanel2Layout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.jLabel3)
						.addPreferredGap(
								LayoutStyle.ComponentPlacement.RELATED, 63,
								32767).addComponent(this.jLabel1)
						.addContainerGap()));

		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGroup(
				jPanel2Layout
						.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(this.jLabel1)
						.addComponent(this.jLabel3, -1, 32, 32767)));

		getContentPane().add(this.jPanel2, "First");

		pack();
	}

	
	
	private void jButton2ActionPerformed(ActionEvent evt) {
		int returnVal = this.jFileChooser1.showOpenDialog(this);
		if (returnVal == 0) {
			this.jTextField1.setText(this.jFileChooser1.getSelectedFile()
					.toString());
		}
	}
	private void jRadioButton1ActionPerformed(ActionEvent evt) {
		if (this.jRadioButton2.isSelected())
			this.jRadioButton2.setSelected(false);
	}

	private void jRadioButton2ActionPerformed(ActionEvent evt) {
		if (this.jRadioButton1.isSelected())
			this.jRadioButton1.setSelected(false);
	}
}