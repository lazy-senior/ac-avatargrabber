import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


public class AvatarGrabber {
	
	private AvatarGrabberGUI gui;
	
	private String ffCachePath;
	private String chromeCachePath;
	private String regex;
	
	public AvatarGrabber(){
		regex = "(http:\\/\\/cdn.mangachat.eu\\/avatars\\/\\d*\\/\\d*\\.png)";
		
		ffCachePath = System.getenv("APPDATA")
						.replaceAll("\\\\Roaming", "")
						.concat("\\Local\\Mozilla\\Firefox\\Profiles\\");
		chromeCachePath = System.getenv("APPDATA")
						.replaceAll("\\\\Roaming", "")
						.concat("\\Local\\Google\\Chrome\\User Data\\Default\\Cache\\data_1");
		
		gui = new AvatarGrabberGUI();
		gui.jButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				grabAction(evt);
			}
		});
		gui.setVisible(true);
	}

	private String getFirefoxCache() throws Exception{
		String cache = "";
		String path = "";
		File f = new File(ffCachePath);
		for (int i = 0; f.list().length > i; i++) {
			path = new StringBuilder().append(ffCachePath).append(f.list()[0]).append("\\Cache\\_CACHE_001_").toString();
			System.out.println(path);
			cache = cache + getFileContents(path);
		}
		return cache;
		
	}
	
	private String getChromeCache() throws Exception{
		System.out.println(chromeCachePath);
		return getFileContents(chromeCachePath);
	}
	
	
	private void grabAction(ActionEvent evt) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				grab();
			}
		}).start();
	}
	
	private void grab() {
		String cache = "";

		int newPics = 0, oldPics = 0;
		gui.jProgressBar1.setMinimum(0);
		gui.jProgressBar1.setValue(0);
		gui.jTextField2.setText("");
		try {
			if (((!gui.jTextField1.getText().isEmpty()) && (gui.jRadioButton1.isSelected())) || (gui.jRadioButton2.isSelected())) {
				String savedir = gui.jTextField1.getText() + "\\";
								
				//Firefox
				if (gui.jRadioButton1.isSelected()) {
					try {
						cache = getFirefoxCache();
					} catch (Exception e) {
					}
				}
				
				//Chrome
				if (gui.jRadioButton2.isSelected()) {
					try {
						cache = getChromeCache();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}

				String[] cacheentrys = cache.split(regex);
				Pattern pNO = Pattern.compile(regex);
				Matcher m = pNO.matcher(cache);
				for (int i = 0; m.find(); i++) {
					cacheentrys[i] = m.group(1);
				}
				for (int i = 0; i < cacheentrys.length - 1; i++) {
					gui.jProgressBar1.setMaximum(cacheentrys.length - 1);

					String userNumber = "unknown";
					String picNumber = "unknown" + i;

					BufferedImage im;
					
					URL url = new URL(cacheentrys[i]);

					pNO = Pattern.compile("/(\\d{1,5})/(\\d*\\.png)");
					m = pNO.matcher(cacheentrys[i]);

					if (m.find()) {
						userNumber = m.group(1);
						picNumber = m.group(2);
					
						System.out.print(userNumber + "\\" + picNumber);
						
						
						if (!new File(savedir).exists()) {
							System.out.print(" 1 ");
							new File(savedir).mkdir();
						}
						if (!new File(savedir+"original\\").exists()) {
							System.out.print(" 1 ");
							new File(savedir+"original\\").mkdir();
						}
						if(!new File(savedir+"upload\\").exists()){
							new File(savedir+"upload\\").mkdir();
						}
						if (!new File(savedir+"original\\"+ userNumber).exists()) {
							System.out.println(" 2 ");
							new File(savedir+"original\\"+ userNumber).mkdir();
						}
						if (!new File(savedir+"upload\\"+userNumber).exists()) {
							System.out.println(" 2 ");
							new File(savedir+"upload\\"+userNumber).mkdir();
						}
						if (!new File(savedir+"original\\" + userNumber + "\\" + picNumber).exists()) {
							System.out.println(" 3 ");
							newPics++;
							InputStream in = url.openStream();
							OutputStream out = new BufferedOutputStream(new FileOutputStream(savedir +"original\\" + userNumber + "\\" + picNumber));
							int b;
							while ((b = in.read()) != -1) {
								out.write(b);
							}
							out.close();
							in.close();
							
							im = ImageIO.read(new File(savedir +"original\\" + userNumber + "\\" + picNumber));
							ImageIO.write(im.getSubimage(1, 1, im.getWidth()-2, im.getHeight()-2), "png", new File(savedir + "upload\\" + userNumber + "\\" + picNumber));
						}else{
							System.out.println(" 4 ");
							oldPics++;
						}
						System.out.println();
					}
					gui.jProgressBar1.setValue(i);
				}

				gui.jTextField2.setText(newPics + " new, " + oldPics + " old");
			} else {
				gui.jTextField2.setText("Something is missing!!");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	private String getFileContents(String path) throws Exception {
		File fa = new File(path);
		InputStream in = new FileInputStream(fa);
		byte[] b = new byte[(int) fa.length()];
		int len = b.length;
		int total = 0;

		while (total < len) {
			int result = in.read(b, total, len - total);
			if (result == -1) {
				break;
			}
			total += result;
		}
		in.close();
		return new String(b);
	}

	public static void main(String[] args) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()){
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(AvatarGrabberGUI.class.getName()).log(Level.SEVERE, null,ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(AvatarGrabberGUI.class.getName()).log(Level.SEVERE, null,ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(AvatarGrabberGUI.class.getName()).log(Level.SEVERE, null,ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(AvatarGrabberGUI.class.getName()).log(Level.SEVERE, null,ex);
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new AvatarGrabber();
			}
		});
	}
}
